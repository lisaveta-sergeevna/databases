set nocount on
go

print 'creating database'

if exists(select 1 from sys.databases where name = 'PlantCatalog')
	drop database PlantCatalog
go

create database PlantCatalog
go

:On Error exit

--Constraints creation procedures

:r .\DDL\Procedures\Constraints\usp_add_alter_key_constraint.sql
print 'usp_add_alter_key_constraint created'
:r .\DDL\Procedures\Constraints\usp_add_check_constraint.sql
print 'usp_add_check_constraint created'
:r .\DDL\Procedures\Constraints\usp_add_default_constraint.sql
print 'usp_add_default_constraint created'
:r .\DDL\Procedures\Constraints\usp_add_foreign_key_constraint.sql
print 'usp_add_foreign_key_constraint created'

--Tables

:r .\DDL\Tables\clade.sql
print 'clade table created'
:r .\DDL\Tables\family.sql
print 'family table created'
:r .\DDL\Tables\genus.sql
print 'genus table created'
:r .\DDL\Tables\group.sql
print 'group table created'
:r .\DDL\Tables\image.sql
print 'image table created'
:r .\DDL\Tables\plant.sql
print 'plant table created'
:r .\DDL\Tables\plant_soil_bridge.sql
print 'plant soil bridge table created'
:r .\DDL\Tables\review.sql
print 'review table created'
:r .\DDL\Tables\role.sql
print 'role table created'
:r .\DDL\Tables\soil.sql
print 'soil table created'
:r .\DDL\Tables\user.sql
print 'user table created'
:r .\DDL\Tables\user_plant.sql
print 'user plant table created'
:r .\DDL\Tables\watering_plan.sql
print 'watering plan table created'

--Table constraints

:r .\DDL\Constraints\clade_constraints.sql
print 'clade constraints created'
:r .\DDL\Constraints\family_constraints.sql
print 'family constraints created'
:r .\DDL\Constraints\genus_constraints.sql
print 'genus constraints created'
:r .\DDL\Constraints\group_constraints.sql
print 'group constraints created'
:r .\DDL\Constraints\image_constraints.sql
print 'image constraints created'
:r .\DDL\Constraints\plant_constraints.sql
print 'plant constraints created'
:r .\DDL\Constraints\plantsoilbridge_constraints.sql
print 'plant soil bridge constraints created'
:r .\DDL\Constraints\review_constraints.sql
print 'review constraints created'
:r .\DDL\Constraints\role_constraints.sql
print 'role constraints created'
:r .\DDL\Constraints\soil_constraints.sql
print 'soil constraints created'
:r .\DDL\Constraints\user_constraints.sql
print 'user constraints created'
:r .\DDL\Constraints\userplant_constraints.sql
print 'userplant constraints created'
:r .\DDL\Constraints\wateringplan_constraints.sql
print 'wateringplan constraints created'

--JSON parsing procedures

:r .\DDL\Procedures\Parsers\usp_parse_json_add_clade.sql
print 'usp_parse_json_add_clade created'
:r .\DDL\Procedures\Parsers\usp_parse_json_add_family.sql
print 'usp_parse_json_add_family created'
:r .\DDL\Procedures\Parsers\usp_parse_json_add_genus.sql
print 'usp_parse_json_add_genus created'
:r .\DDL\Procedures\Parsers\usp_parse_json_add_group.sql
print 'usp_parse_json_add_group created'
:r .\DDL\Procedures\Parsers\usp_parse_json_add_plant.sql
print 'usp_parse_json_add_plant created'
:r .\DDL\Procedures\Parsers\usp_parse_json_add_plant_to_list.sql
print 'usp_parse_json_add_plant_to_list created'
:r .\DDL\Procedures\Parsers\usp_parse_json_add_review.sql
print 'usp_parse_json_add_review created'
:r .\DDL\Procedures\Parsers\usp_parse_json_add_role.sql
print 'usp_parse_json_add_role created'
:r .\DDL\Procedures\Parsers\usp_parse_json_add_soil.sql
print 'usp_parse_json_add_soil created'
:r .\DDL\Procedures\Parsers\usp_parse_json_add_watering_plan.sql
print 'usp_parse_json_add_watering_plan created'
:r .\DDL\Procedures\Parsers\usp_parse_json_illustrate.sql
print 'usp_parse_json_illustrate created'
:r .\DDL\Procedures\Parsers\usp_parse_json_planted_in.sql
print 'usp_parse_json_planted_in created'
:r .\DDL\Procedures\Parsers\usp_parse_json_remove_review.sql
print 'usp_parse_json_remove_review created'
:r .\DDL\Procedures\Parsers\usp_parse_json_sign_in.sql
print 'usp_parse_json_sign_in created'
:r .\DDL\Procedures\Parsers\usp_parse_json_sign_up.sql
print 'usp_parse_json_sign_up created'

--Load data from JSONs

:r .\DML\LoadJsonData\load_groups.sql
print 'groups loaded'
:r .\DML\LoadJsonData\load_clades.sql
print 'clades loaded'
:r .\DML\LoadJsonData\load_families.sql
print 'families loaded'
:r .\DML\LoadJsonData\load_genuses.sql
print 'genuses loaded'
:r .\DML\LoadJsonData\load_plants.sql
print 'plants loaded'
:r .\DML\LoadJsonData\load_images.sql
print 'images loaded'
:r .\DML\LoadJsonData\load_soils.sql
print 'soils loaded'
:r .\DML\LoadJsonData\load_plantsoils.sql
print 'plantsoils loaded'
:r .\DML\LoadJsonData\load_roles.sql
print 'roles loaded'
:r .\DML\LoadJsonData\sign_up_user.sql
print 'user signed up'
:r .\DML\LoadJsonData\load_plant_to_list.sql
print 'user plants loaded'
:r .\DML\LoadJsonData\load_watering_plan.sql
print 'watering plans loaded'
:r .\DML\LoadJsonData\load_reviews.sql
print 'reviews loaded'

print 'database creation complete'
go