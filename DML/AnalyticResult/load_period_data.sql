﻿USE [PlantCatalog]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[USP_GetPlantReviewsByPeriods]
		@json = 
		N'{
			"Family": {
				"Name": "Тутовые"
			},
			"PeriodStart": "2018-02",
			"PeriodEnd": "2021-05",
			"ShowTotalPlants": 0,
			"ShowMaxUserPlants": 0,
			"ShowZeroColumns": 0,
			"ShowZeroRows": 1
		}'

SELECT	'Return Value' = @return_value

GO
