USE [PlantCatalog]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[USP_ParseJson_RemoveReview]
		@json = 
		N'{
			"Review": {
				"PublicationDateTime": "2021-05-13 15:19:18.553"
			},
			"User": {
				"Login": "johnnydoe"
			}
		}'

SELECT	'Return Value' = @return_value

GO
