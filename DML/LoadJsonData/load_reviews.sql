﻿USE [PlantCatalog]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[USP_ParseJson_AddReview]
		@json = 
		N'	{
				"Review": {
					"Text": "Красивое растение, вырастает большим. Чтобы остановить рост, рекомендую не пересаживать некоторое время."
				},
				"Plant": {
					"Name": "Фикус Бенджамина"
				},
				"User": {
					"Login": "johnnydoe"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_AddReview]
		@json = 
		N'	{
				"Review": {
					"Text": "Растение опускает листья при пересадке в слишком большой горшок, рекомендую брать узкий и высокий."
				},
				"Plant": {
					"Name": "Замиокулькас"
				},
				"User": {
					"Login": "annydoe"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_AddReview]
		@json = 
		N'	{
				"Review": {
					"Text": "Растение нуждается во влажности и ярком свете."
				},
				"Plant": {
					"Name": "Фикус Бенджамина"
				},
				"User": {
					"Login": "may-zoe"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_AddReview]
		@json = 
		N'	{
				"Review": {
					"Text": "Рекомендую пересаживать растение достаточно часто."
				},
				"Plant": {
					"Name": "Фикус лировидный"
				},
				"User": {
					"Login": "may-zoe"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_AddReview]
		@json = 
		N'	{
				"Review": {
					"Text": "Плоды растения можно употреблять в пищу."
				},
				"Plant": {
					"Name": "Физалис перуанский"
				},
				"User": {
					"Login": "valentine90"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_AddReview]
		@json = 
		N'	{
				"Review": {
					"Text": "При регулярном опрыскивании проблемы с опадающей листвой исчезли."
				},
				"Plant": {
					"Name": "Фикус Бенджамина"
				},
				"User": {
					"Login": "may-zoe"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_AddReview]
		@json = 
		N'	{
				"Review": {
					"Text": "Растение плохо относится к переливанию."
				},
				"Plant": {
					"Name": "Замиокулькас"
				},
				"User": {
					"Login": "annydoe"
				}
			}'

SELECT	'Return Value' = @return_value
GO