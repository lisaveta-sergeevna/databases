﻿USE [PlantCatalog]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[USP_ParseJson_AddClade]
		@json = 
		N'[
			{
				"Clade": {
					"Name": "Двудольные",
					"LatinName": "Magnoliopsida"
				},
				"Group": {
					"Name": "Семенные"
				}
			},
			{
				"Clade": {
					"Name": "Однодольные",
					"LatinName": "Liliopsida Batsch"
				},
				"Group": {
					"Name": "Семенные"
				}
			},
			{
				"Clade": {
					"Name": "Папоротниковые",
					"LatinName": "Polypodiopsida Cronquist, Takht. & W.Zimm."
				},
				"Group": {
					"Name": "Споровые"
				}
			},
			{
				"Clade": {
					"Name": "Плауновидные",
					"LatinName": "Lycopodiophyta D.H.Scott"
				},
				"Group": {
					"Name": "Споровые"
				}
			}
		]'

SELECT	'Return Value' = @return_value

GO
