﻿USE [PlantCatalog]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[USP_ParseJson_AddGroup]
		@json = 
		N'[
			{
				"Name": "Водоросли",
				"LatinName": "Algae"
			},
			{
				"Name": "Споровые",
				"LatinName": "Pteridophyta"
			},
			{
				"Name": "Мохообразные",
				"LatinName": "Bryophyta sensu lato"
			},
			{
				"Name": "Семенные",
				"LatinName": "Spermatophyta"
			}
		]'

SELECT	'Return Value' = @return_value

GO
