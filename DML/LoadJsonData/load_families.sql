﻿USE [PlantCatalog]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[USP_ParseJson_AddFamily]
		@json = 
		N'[
			{
				"Family": {
					"Name": "Тутовые",
					"LatinName": "Moraceae"
				},
				"Clade": {
					"Name":  "Двудольные"
				}
			},
			{
				"Family": {
					"Name": "Ароидные",
					"LatinName": "Araceae Juss."
				},
				"Clade": {
					"Name": "Однодольные"
				}
			},
			{
				"Family": {
					"Name": "Нефролеписовые",
					"LatinName": "Nephrolepidaceae"
				},
				"Clade": {
					"Name": "Папоротниковые"
				}
			},
			{
				"Family": {
					"Name": "Полушниковые",
					"LatinName": "Isoëtopsida Rolle"
				},
				"Clade": {
					"Name": "Плауновидные"
				}
			},
			{
				"Family": {
					"Name": "Паслёновые",
					"LatinName": "Solanaceae Juss., nom. cons."
				},
				"Clade": {
					"Name": "Двудольные"
				}
			},
			{
				"Family": {
					"Name": "Орхидеи",
					"LatinName": "Orchidaceae Juss."
				},
				"Clade": {
					"Name": "Однодольные"
				}
			}
		]'

SELECT	'Return Value' = @return_value

GO
