﻿USE [PlantCatalog]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[USP_ParseJson_Illustrate]
		@json = 
		N'[
			{
				"Image": {
					"Url": "https://images.app.goo.gl/zGVsPcoKDDhSDGGr6"
				},
				"Plant": {
					"Name": "Фикус лировидный"
				}
			},
			{
				"Image": {
					"Url": "https://jungle.by/sites/default/files/styles/product/public/products/img_7822.jpg?itok=Hbpy7Xaf"
				},
				"Plant": {
					"Name": "Фикус лировидный"
				}
			},
			{
				"Image": {
					"Url": "https://recommerce.gumlet.io/villama1.reshop.by/catalog/292/1698749115f57592e445e4_original.jpg?enlarge=true&mode=fit&width=1200&format=auto"
				},
				"Plant": {
					"Name": "Фикус лировидный"
				}
			},
			{
				"Image": {
					"Url": "https://stroy-podskazka.ru/images/article/croppedtop/718-400/2019/01/pletenie-fikusa-bendzhamina-vidy-pravila-pleteniya-i-uhoda.jpg"
				},
				"Plant": {
					"Name": "Фикус Бенджамина"
				}
			},
			{
				"Image": {
					"Url": "https://rastenievod.com/wp-content/uploads/2016/05/3-77.jpg"
				},
				"Plant": {
					"Name": "Фикус Бенджамина"
				}
			},
			{
				"Image": {
					"Url": "https://rastenievod.com/wp-content/uploads/2016/08/2-50-700x678.jpg"
				},
				"Plant": {
					"Name": "Фикус каучуконосный"
				}
			},
			{
				"Image": {
					"Url": "https://rastenievod.com/wp-content/uploads/2016/08/4-44.jpg"
				},
				"Plant": {
					"Name": "Фикус каучуконосный"
				}
			},
			{
				"Image": {
					"Url": "https://upload.wikimedia.org/wikipedia/commons/thumb/6/61/Phalaenopsis_lowii_Orchi_914.jpg/1200px-Phalaenopsis_lowii_Orchi_914.jpg"
				},
				"Plant": {
					"Name": "Фаленопсис Лоу"
				}
			},
			{
				"Image": {
					"Url": "https://rastenievod.com/wp-content/uploads/2016/08/2-61-700x700.jpg"
				},
				"Plant": {
					"Name": "Замиокулькас"
				}
			},
			{
				"Image": {
					"Url": "https://ireland.apollo.olxcdn.com/v1/files/s2rgb33vfz07-UA/image;s=1000x700"
				},
				"Plant": {
					"Name": "Замиокулькас"
				}
			},
			{
				"Image": {
					"Url": "https://flowertimes.ru/wp-content/uploads/2016/10/selaginella_plaunok.jpg"
				},
				"Plant": {
					"Name": "Селагинелла Мёллендорфа"
				}
			}
		]'

SELECT	'Return Value' = @return_value

GO
