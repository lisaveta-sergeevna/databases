﻿USE [PlantCatalog]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[USP_ParseJson_PlantedIn]
		@json = 
		N'[
			{
				"Plant": {
					"Name": "Фикус лировидный"
				},
				"Soil": {
					"Name": "Дерновая",
					"Percent": 60
				}
			},
			{
				"Plant": {
					"Name": "Фикус лировидный"
				},
				"Soil": {
					"Name": "Кокосовое волокно",
					"Percent": 20
				}
			},
			{
				"Plant": {
					"Name": "Фикус лировидный"
				},
				"Soil": {
					"Name": "Перлит",
					"Percent": 20
				}
			},
			{
				"Plant": {
					"Name": "Фикус каучуконосный"
				},
				"Soil": {
					"Name": "Дерновая",
					"Percent": 100
				}
			},
			{
				"Plant": {
					"Name": "Фикус Бенджамина"
				},
				"Soil": {
					"Name": "Перлит",
					"Percent": 30
				}
			},
			{
				"Plant": {
					"Name": "Антуриум Андре"
				},
				"Soil": {
					"Name": "Хвойная",
					"Percent": 50
				}
			},
			{
				"Plant": {
					"Name": "Антуриум Андре"
				},
				"Soil": {
					"Name": "Перегной",
					"Percent": 50
				}
			},
			{
				"Plant": {
					"Name": "Замиокулькас"
				},
				"Soil": {
					"Name": "Дерновая",
					"Percent": 50
				}
			},
			{
				"Plant": {
					"Name": "Селагинелла Мёллендорфа"
				},
				"Soil": {
					"Name": "Хвойная",
					"Percent": 90
				}
			},
			{
				"Plant": {
					"Name": "Селагинелла Мёллендорфа"
				},
				"Soil": {
					"Name": "Торфяная",
					"Percent": 10
				}
			},
			{
				"Plant": {
					"Name": "Нефролепис возвышенный"
				},
				"Soil": {
					"Name": "Листовая",
					"Percent": 80
				}
			},
			{
				"Plant": {
					"Name": "Фаленопсис Шиллера"
				},
				"Soil": {
					"Name": "Кокосовое волокно",
					"Percent": 70
				}
			}
		]'

SELECT	'Return Value' = @return_value

GO
