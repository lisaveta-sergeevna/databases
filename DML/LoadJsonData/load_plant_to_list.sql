﻿USE [PlantCatalog]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[USP_ParseJson_AddPlantToList]
		@json = 
		N'	{
				"UserPlant": {
					"Number": 1,
					"PlantDate": "2021-01-10",
					"Name": "Фикус Алёша",
					"PotVolume": 3
				},
				"User": {
					"Login": "johnnydoe"
				},
				"Plant": {
					"Name": "Фикус лировидный"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_AddPlantToList]
		@json = 
		N'	{
				"UserPlant": {
					"Number": 2,
					"PlantDate": "2021-01-10",
					"Name": "Фикус Андрей",
					"PotVolume": 2
				},
				"User": {
					"Login": "johnnydoe"
				},
				"Plant": {
					"Name": "Фикус лировидный"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_AddPlantToList]
		@json = 
		N'	{
				"UserPlant": {
					"Number": 1,
					"PlantDate": "2021-02-10",
					"Name": "Фикус Джон",
					"PotVolume": 2
				},
				"User": {
					"Login": "annydoe"
				},
				"Plant": {
					"Name": "Фикус лировидный"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_AddPlantToList]
		@json = 
		N'	{
				"UserPlant": {
					"Number": 3,
					"PlantDate": "2021-01-10",
					"Name": "Какой-то фикус",
					"PotVolume": 3
				},
				"User": {
					"Login": "johnnydoe"
				},
				"Plant": {
					"Name": "Фикус лировидный"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_AddPlantToList]
		@json = 
		N'	{
				"UserPlant": {
					"Number": 1,
					"PlantDate": "2021-01-19",
					"Name": "Растение",
					"PotVolume": 5
				},
				"User": {
					"Login": "annydoe"
				},
				"Plant": {
					"Name": "Фикус Бенджамина"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_AddPlantToList]
		@json = 
		N'	{
				"UserPlant": {
					"Number": 1,
					"PlantDate": "2021-01-31",
					"Name": "Растение",
					"PotVolume": 8
				},
				"User": {
					"Login": "annydoe"
				},
				"Plant": {
					"Name": "Антуриум Андре"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_AddPlantToList]
		@json = 
		N'	{
				"UserPlant": {
					"Number": 1,
					"PlantDate": "2021-04-03",
					"Name": "Вениамин",
					"PotVolume": 13
				},
				"User": {
					"Login": "may-zoe"
				},
				"Plant": {
					"Name": "Нефролепис возвышенный"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_AddPlantToList]
		@json = 
		N'	{
				"UserPlant": {
					"Number": 1,
					"PlantDate": "2021-05-23",
					"Name": "Цветочек на верхней полке",
					"PotVolume": 1
				},
				"User": {
					"Login": "may-zoe"
				},
				"Plant": {
					"Name": "Фаленопсис Лоу"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_AddPlantToList]
		@json = 
		N'	{
				"UserPlant": {
					"Number": 1,
					"PlantDate": "2021-05-23",
					"Name": "Барт",
					"PotVolume": 4
				},
				"User": {
					"Login": "valentine90"
				},
				"Plant": {
					"Name": "Физалис перуанский"
				}
			}'

SELECT	'Return Value' = @return_value

GO
