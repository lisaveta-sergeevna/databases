USE [PlantCatalog]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[USP_ParseJson_SignIn]
		@json = 
		N'{
			"User": {
				"Login": "johnnydoe",
				"PasswordHash": "65e84be33532fb784c48129675f9eff3a682b27168c0ea744b2cf58ee02337c5"
			}
		}'

SELECT	'Return Value' = @return_value

GO
