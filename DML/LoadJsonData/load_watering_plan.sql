﻿USE [PlantCatalog]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[USP_ParseJson_AddWateringPlan]
		@json = 
		N'	{
				"WateringPlan": {
					"Number": 1,
					"WaterVolume": 30,
					"Description": "Полив отстоянной водой",
					"StartDateTime": "2021-05-12T10:00:00.0",
					"Period": 5,
					"NotificationsEnabled": 1
				},
				"UserPlant": {
					"Number": 1
				},
				"Plant": {
					"Name": "Фикус лировидный"
				},
				"User": {
					"Login": "johnnydoe"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_AddWateringPlan]
		@json = 
		N'	{
				"WateringPlan": {
					"Number": 2,
					"WaterVolume": 60,
					"Description": "Полив кислым удобрением",
					"StartDateTime": "2021-05-12T12:00:00.0",
					"Period": 10,
					"NotificationsEnabled": 1
				},
				"UserPlant": {
					"Number": 1
				},
				"Plant": {
					"Name": "Фикус лировидный"
				},
				"User": {
					"Login": "johnnydoe"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_AddWateringPlan]
		@json = 
		N'	{
				"WateringPlan": {
					"Number": 1,
					"WaterVolume": 70,
					"Description": "Полив в поддон растения",
					"StartDateTime": "2021-05-15T14:00:00.0",
					"Period": 10,
					"NotificationsEnabled": 0
				},
				"UserPlant": {
					"Number": 2
				},
				"Plant": {
					"Name": "Фикус лировидный"
				},
				"User": {
					"Login": "johnnydoe"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_AddWateringPlan]
		@json = 
		N'	{
				"WateringPlan": {
					"Number": 1,
					"WaterVolume": 100,
					"Description": "Вода + минералы",
					"StartDateTime": "2021-05-18T11:30:00.0",
					"Period": 6,
					"NotificationsEnabled": 1
				},
				"UserPlant": {
					"Number": 1
				},
				"Plant": {
					"Name": "Фикус Бенджамина"
				},
				"User": {
					"Login": "annydoe"
				}
			},
			'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_AddWateringPlan]
		@json = 
		N'	{
				"WateringPlan": {
					"Number": 1,
					"WaterVolume": 150,
					"Description": "Кислые добавки",
					"StartDateTime": "2021-05-20T14:30:00.0",
					"Period": 6,
					"NotificationsEnabled": 0
				},
				"UserPlant": {
					"Number": 1
				},
				"Plant": {
					"Name": "Нефролепис возвышенный"
				},
				"User": {
					"Login": "may-zoe"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_AddWateringPlan]
		@json = 
		N'	{
				"WateringPlan": {
					"Number": 1,
					"WaterVolume": 110,
					"Description": "Вода",
					"StartDateTime": "2021-05-20T14:30:00.0",
					"Period": 5,
					"NotificationsEnabled": 0
				},
				"UserPlant": {
					"Number": 1
				},
				"Plant": {
					"Name": "Фаленопсис Лоу"
				},
				"User": {
					"Login": "may-zoe"
				}
			}'

SELECT	'Return Value' = @return_value

GO
