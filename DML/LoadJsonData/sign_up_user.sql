USE [PlantCatalog]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[USP_ParseJson_SignUp]
		@json = 
		N'	{
				"User": {
					"Name": "John Doe",
					"Email": "johndoe@gmail.com",
					"Login": "johnnydoe",
					"PasswordHash": "65e84be33532fb784c48129675f9eff3a682b27168c0ea744b2cf58ee02337c5"
				},
				"Role": {
					"Name": "client"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_SignUp]
		@json = 
		N'	{
				"User": {
					"Name": "Anna Doe",
					"Email": "annadoe@gmail.com",
					"Login": "annydoe",
					"PasswordHash": "34e84be33532fb784c48129675f9eff3a682b27168c0ea744b2cf58ee02337c5"
				},
				"Role": {
					"Name": "client"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_SignUp]
		@json = 
		N'	{
				"User": {
					"Name": "Zoe May",
					"Email": "may33@gmail.com",
					"Login": "may-zoe",
					"PasswordHash": "65e84be33532fb784c48129675f9eff3a682b27168c0ea744b2cf58ee02337c4"
				},
				"Role": {
					"Name": "client"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_SignUp]
		@json = 
		N'	{
				"User": {
					"Name": "George Hansen",
					"Email": "j.hanse@gmail.com",
					"Login": "j.hansen",
					"PasswordHash": "65e67be33532fb784c48129675f9eff3a682b27168c0ea744b2cf58ee02337c5"
				},
				"Role": {
					"Name": "admin"
				}
			}'

SELECT	'Return Value' = @return_value

EXEC	@return_value = [dbo].[USP_ParseJson_SignUp]
		@json = 
		N'
			{
				"User": {
					"Name": "Valetine McKay",
					"Email": "mckayvalentine@gmail.com",
					"Login": "valentine90",
					"PasswordHash": "65e44be33532fb784c48129675f9eff3a682b27168c0ea744b2cf58ee02337c5"
				},
				"Role": {
					"Name": "client"
				}
			}'

SELECT	'Return Value' = @return_value

GO


