﻿USE [PlantCatalog]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[USP_ParseJson_AddGenus]
		@json = 
		N'[
			{
				"Genus": {
					"Name": "Фикус",
					"LatinName": "Ficus L."
				},
				"Family": {
					"Name": "Тутовые"
				}
			},
			{
				"Genus": {
					"Name": "Замиокулькас",
					"LatinName": "Zamioculcas Schott"
				},
				"Family": {
					"Name": "Ароидные"
				}
			},
			{
				"Genus": {
					"Name": "Нефролепис",
					"LatinName": "Nephrolepis Schott"
				},
				"Family": {
					"Name": "Нефролеписовые"
				}
			},
			{
				"Genus": {
					"Name": "Селагинелла",
					"LatinName": "Selaginella P.Beauv."
				},
				"Family": {
					"Name": "Полушниковые"
				}
			},
			{
				"Genus": {
					"Name": "Антуриум",
					"LatinName": "Anthurium Schott"
				},
				"Family": {
					"Name": "Ароидные"
				}
			},
			{
				"Genus": {
					"Name": "Физалис",
					"LatinName": "Physalis L."
				},
				"Family": {
					"Name": "Паслёновые"
				}
			},
			{
				"Genus": {
					"Name": "Фаленопсис",
					"LatinName": "Phalaenopsis Blume"
				},
				"Family": {
					"Name": "Охридеи"
				}	
			}
		]'

SELECT	'Return Value' = @return_value

GO
