USE [PlantCatalog]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[USP_ParseJson_AddRole]
		@json = 
		N'[
			{
				"Name": "client"
			}, 
			{
				"Name": "admin"
			}
		]'

SELECT	'Return Value' = @return_value

GO
