﻿USE [PlantCatalog]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[USP_ParseJson_AddPlant]
		@json = 
		N'[
			{
				"Plant": {
					"Name": "Фикус лировидный",
					"LatinName": "Ficus lyrata",
					"Illumination": "Прямой или рассеянный солнечный свет",
					"AirHumidity": 70,
					"Temperature": 25,
					"RecommendedWateringPeriod": 4,
					"RecommendedWateringVolume": 10
				},
				"Genus": {
					"Name": "Фикус"
				}
			},
			{
				"Plant": {
					"Name": "Фикус каучуконосный",
					"LatinName": "Ficus elastica",
					"Illumination": "Умеренное затенение или рассеянный свет",
					"AirHumidity": 55,
					"Temperature": 22,
					"RecommendedWateringPeriod": 7,
					"RecommendedWateringVolume": 30
				},
				"Genus": {
					"Name": "Фикус"
				}
			},
			{
				"Plant": {
					"Name": "Фикус Бенджамина",
					"LatinName": "Ficus benjamina",
					"Illumination": "Умеренное затенение или рассеянный свет",
					"AirHumidity": 55,
					"Temperature": 23,
					"RecommendedWateringPeriod": 4,
					"RecommendedWateringVolume": 20
				},
				"Genus": {
					"Name": "Фикус"
				}
			},
			{
				"Plant": {
					"Name": "Фикус микрокарпа",
					"LatinName": "Ficus microcarpa",
					"Illumination": "Большое количество рассеянного солнечного света",
					"AirHumidity": 65,
					"Temperature": 20,
					"RecommendedWateringPeriod": 4,
					"RecommendedWateringVolume": 22
				},
				"Genus": {
					"Name": "Фикус"
				}
			},
			{
				"Plant": {
					"Name": "Замиокулькас",
					"LatinName": "Caladium zamiaefolium",
					"Illumination": "Прямой и яркий солнечный свет в больших количествах",
					"AirHumidity": 50,
					"Temperature": 22,
					"RecommendedWateringPeriod": 7,
					"RecommendedWateringVolume": 20
				},
				"Genus": {
					"Name": "Замиокулькас"
				}
			},
			{
				"Plant": {
					"Name": "Нефролепис возвышенный",
					"LatinName": "Nephrolepis exaltata",
					"Illumination": "Яркое рассеянное освещение, летом избегать прямых солнечных лучей",
					"AirHumidity": 60,
					"Temperature": 20,
					"RecommendedWateringPeriod": 5,
					"RecommendedWateringVolume": 25
				},
				"Genus": {
					"Name": "Нефролепис"
				}
			},
			{
				"Plant": {
					"Name": "Селагинелла Мёллендорфа",
					"LatinName": "Selaginélla moellendórffii",
					"Illumination": "Рассеянный солнечный свет, искусственное освещение",
					"AirHumidity": 75,
					"Temperature": 19,
					"RecommendedWateringPeriod": 3,
					"RecommendedWateringVolume": 15
				},
				"Genus": {
					"Name": "Селагинелла"
				}
			},
			{
				"Plant": {
					"Name": "Антуриум Андре",
					"LatinName": "Anthúrium andraéanum",
					"Illumination": "Рассеянный солнечный свет, искусственное освещение",
					"AirHumidity": 80,
					"Temperature": 23,
					"RecommendedWateringPeriod": 4,
					"RecommendedWateringVolume": 23
				},
				"Genus": {
					"Name": "Антуриум"
				}
			},
			{
				"Plant": {
					"Name": "Физалис перуанский",
					"LatinName": "Physalis peruviana L.",
					"Illumination": "Прямой солнечный свет",
					"AirHumidity": 60,
					"Temperature": 17,
					"RecommendedWateringPeriod": 7,
					"RecommendedWateringVolume": 34
				},
				"Genus": {
					"Name": "Физалис"
				}
			},
			{
				"Plant": {
					"Name": "Фаленопсис Лоу",
					"LatinName": "Phalaenopsis lowii",
					"Illumination": "Рассеянный солнечный свет с обязательным доступом света к корням",
					"AirHumidity": 70,
					"Temperature": 26,
					"RecommendedWateringPeriod": 2,
					"RecommendedWateringVolume": 14
				},
				"Genus": {
					"Name": "Фаленопсис"
				}
			},
			{
				"Plant": {
					"Name": "Фаленопсис Шиллера",
					"LatinName": "Phalaenopsis schilleriana",
					"Illumination": "Рассеянный солнечный свет с обязательным доступом света к корням",
					"AirHumidity": 75,
					"Temperature": 24,
					"RecommendedWateringPeriod": 2,
					"RecommendedWateringVolume": 14
				},
				"Genus": {
					"Name": "Фаленопсис"
				}
			}
		]'

SELECT	'Return Value' = @return_value

GO
