use PlantCatalog
go

if object_id(N'dbo.Plant', N'U') is not null
	drop table dbo.Plant
go

create table dbo.Plant
(
	PlantId						uniqueidentifier	not null	constraint DF_Plant_PlantId default newid(),
	GenusId						uniqueidentifier	not null,
	[Name]						nvarchar(64)		not null,
	LatinName					nvarchar(64)		not null,
	Illumination				nvarchar(512)		not null,
	AirHumidity					float				not null,
	Temperature					float				not null,
	RecommendedWateringPeriod	tinyint				not null,
	RecommendedWateringVolume	tinyint				not null,

	constraint PK_Plant primary key (
        PlantId
    )
)
go