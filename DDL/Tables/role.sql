use PlantCatalog
go

if object_id(N'dbo.Role', N'U') is not null
	drop table dbo.[Role]
go

create table dbo.[Role]
(
	RoleId	uniqueidentifier	not null	constraint DF_Role_RoleId default newid(),
	[Name]	nvarchar(24)		not null,

	constraint PK_Role primary key (
        RoleId
    )
)
go