use PlantCatalog
go

if object_id(N'dbo.Clade', N'U') is not null
	drop table dbo.Clade
go

create table dbo.Clade
(
	CladeId		uniqueidentifier	not null	constraint DF_Clade_CladeId default newid(),
	GroupId		uniqueidentifier	not null,
	[Name]		nvarchar(64)		not null,
	LatinName	nvarchar(64)		not null,

	constraint PK_Clade primary key (
        CladeId
    )
)
go