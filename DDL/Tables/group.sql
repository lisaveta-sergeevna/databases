use PlantCatalog
go

if object_id(N'dbo.Group', N'U') is not null
	drop table dbo.[Group]
go

create table dbo.[Group]
(
	GroupId		uniqueidentifier	not null	constraint DF_Group_GroupId default newid(),
	[Name]		nvarchar(64)		not null,
	LatinName	nvarchar(64)		not null,

	constraint PK_Group primary key (
        GroupId
    )
)
go