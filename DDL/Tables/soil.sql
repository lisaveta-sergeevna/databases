use PlantCatalog
go

if object_id(N'dbo.Soil', N'U') is not null
	drop table dbo.Soil
go

create table dbo.Soil
(
	SoilId			uniqueidentifier	not null	constraint DF_Soil_SoilId default newid(),
	[Name]			nvarchar(64)		not null,
	[Description]	nvarchar(1024)		null,

	constraint PK_Soil primary key (
        SoilId
    )
)
go