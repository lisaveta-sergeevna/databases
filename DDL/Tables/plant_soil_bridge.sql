use PlantCatalog
go

if object_id(N'dbo.PlantSoilBridge', N'U') is not null
	drop table dbo.PlantSoilBridge
go

create table dbo.PlantSoilBridge
(
	PlantId		uniqueidentifier	not null,
	SoilId		uniqueidentifier	not null,
	[Percent]	float				not null,

	constraint PK_PlantSoilBridge primary key (
        PlantId,
		SoilId
    )
)
go