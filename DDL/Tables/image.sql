use PlantCatalog
go

if object_id(N'dbo.Image', N'U') is not null
	drop table dbo.[Image]
go

create table dbo.[Image]
(
	ImageId		uniqueidentifier	not null	constraint DF_Image_ImageId default newid(),
	PlantId		uniqueidentifier	not null,
	[Url]		nvarchar(1024)		not null,

	constraint PK_Image primary key (
        ImageId
    )
)
go