use PlantCatalog
go

if object_id(N'dbo.Review', N'U') is not null
	drop table dbo.Review
go

create table dbo.Review
(
	ReviewId				uniqueidentifier	not null	constraint DF_Review_ReviewId default newid(),
	UserId					uniqueidentifier	not null,
	PlantId					uniqueidentifier	not null,
	PublicationDateTime		datetime2(7)		not null,
	[Text]					nvarchar(2048)		not null,

	constraint PK_Review primary key (
        ReviewId
    )
)
go