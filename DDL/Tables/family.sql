use PlantCatalog
go

if object_id(N'dbo.Family', N'U') is not null
	drop table dbo.Family
go

create table dbo.Family
(
	FamilyId	uniqueidentifier	not null	constraint DF_Family_FamilyId default newid(),
	CladeId		uniqueidentifier	not null,
	[Name]		nvarchar(64)		not null,
	LatinName	nvarchar(64)		not null,

	constraint PK_Family primary key (
        FamilyId
    )
)
go