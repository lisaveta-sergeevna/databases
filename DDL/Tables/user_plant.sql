use PlantCatalog
go

if object_id(N'dbo.UserPlant', N'U') is not null
	drop table dbo.UserPlant
go

create table dbo.UserPlant
(
	UserPlantId		uniqueidentifier	not null	constraint DF_UserPlant_UserPlantId default newid(),
	UserId			uniqueidentifier	not null,
	PlantId			uniqueidentifier	not null,
	Number			tinyint				not null,
	PlantDate		date				not null,
	[Name]			nvarchar(48),
	PotVolume		float,

	constraint PK_UserPlant primary key (
        UserPlantId
    )
)
go