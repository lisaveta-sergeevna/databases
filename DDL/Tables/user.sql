use PlantCatalog
go

if object_id(N'dbo.User', N'U') is not null
	drop table dbo.[User]
go

create table dbo.[User]
(
	UserId			uniqueidentifier	not null	constraint DF_User_UserId default newid(),
	RoleId			uniqueidentifier	not null,
	[Name]			nvarchar(48)		not null,
	[Login]			nvarchar(48)		not null,
	Email			nvarchar(64)		not null,
	PasswordHash	char(64)			not null,
	LastEntryDate	datetime			not null,

	constraint PK_User primary key (
        UserId
    )
)
go