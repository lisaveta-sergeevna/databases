use PlantCatalog
go

if object_id(N'dbo.WateringPlan', N'U') is not null
	drop table dbo.WateringPlan
go

create table dbo.WateringPlan
(
	WateringPlanId			uniqueidentifier	not null	constraint DF_WateringPlan_WateringPlanId default newid(),
	UserPlantId				uniqueidentifier	not null,
	Number					tinyint				not null,
	WaterVolume				int					not null,
	StartDateTime			datetime			not null,
	[Period]				tinyint				not null,
	NotificationsEnabled	bit					not null,
	[Description]			nvarchar(128),

	constraint PK_WateringPlan primary key (
        WateringPlanId
    )
)
go