use PlantCatalog
go

if object_id(N'dbo.Genus', N'U') is not null
	drop table dbo.Genus
go

create table dbo.Genus
(
	GenusId		uniqueidentifier	not null		constraint DF_Genus_GenusId default newid(),
	FamilyId	uniqueidentifier	not null,
	[Name]		nvarchar(64)		not null,
	LatinName	nvarchar(64)		not null,

	constraint PK_Genus primary key (
        GenusId
    )
)
go