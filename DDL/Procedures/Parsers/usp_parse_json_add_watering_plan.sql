use PlantCatalog
go

if object_id('dbo.USP_ParseJson_AddWateringPlan', 'P') is not null
	drop procedure dbo.USP_ParseJson_AddWateringPlan
go

create procedure dbo.USP_ParseJson_AddWateringPlan
(
	@json nvarchar(max)
)
as
begin
	merge	dbo.WateringPlan as [Target]
	using	
	(
		select	jsp.Number					as Number,
				jsp.StartDateTime			as StartDateTime,
				jsp.[Description]			as [Description],
				jsp.WaterVolume				as WaterVolume,
				jsp.[Period]				as [Period],
				jsp.NotificationsEnabled	as NotificationsEnabled,
				up.UserPlantId				as UserPlantId
		from	openjson(@json)
				with
				(
					Number					tinyint			'$.WateringPlan.Number',
					StartDateTime			datetime		'$.WateringPlan.StartDateTime',
					[Description]			nvarchar(128)	'$.WateringPlan.Description',
					WaterVolume				int				'$.WateringPlan.WaterVolume',
					[Period]				tinyint			'$.WateringPlan.Period',
					NotificationsEnabled	bit				'$.WateringPlan.NotificationsEnabled',
					PlantNumber				tinyint			'$.UserPlant.Number',
					PlantName				nvarchar(64)	'$.Plant.Name',
					UserLogin				nvarchar(48)	'$.User.Login'
				) jsp

		inner join	dbo.Plant as p
		on			p.[Name] = jsp.PlantName

		inner join	dbo.[User] as u
		on			u.[Login] = jsp.UserLogin

		inner join	dbo.UserPlant as up
		on			up.UserId = u.UserId
		and			up.PlantId = p.PlantId
		and			up.Number = jsp.PlantNumber
	) as [Source]

	on	[Target].UserPlantId	= [Source].UserPlantId
	and [Target].Number			= [Source].Number

	when matched then
		update
		set [Target].StartDateTime			= [Source].StartDateTime,
			[Target].[Description]			= [Source].[Description],
			[Target].WaterVolume			= [Source].WaterVolume,
			[Target].[Period]				= [Source].[Period],
			[Target].NotificationsEnabled	= [Source].NotificationsEnabled

	when not matched then
		insert
		(
			Number,
			StartDateTime,
			[Description],
			WaterVolume,
			[Period],
			NotificationsEnabled,
			UserPlantId
		)
		values
		(
			Number,
			StartDateTime,
			[Description],
			WaterVolume,
			[Period],
			NotificationsEnabled,
			UserPlantId
		);
end
go