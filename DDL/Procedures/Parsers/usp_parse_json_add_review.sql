use PlantCatalog
go

if object_id('dbo.USP_ParseJson_AddReview', 'P') is not null
	drop procedure dbo.USP_ParseJson_AddReview
go

create procedure dbo.USP_ParseJson_AddReview
(
	@json nvarchar(max)
)
as
begin
	insert into dbo.Review
	(
		[Text],
		PlantId,
		UserId,
		PublicationDateTime
	)
	select	jsr.[Text]		as [Text],
			p.PlantId		as PlantId,
			u.UserId		as UserId,
			sysdatetime()	as PublicationDateTime
	from	openjson(@json)
			with
			(
				[Text]					nvarchar(max)	'$.Review.Text',
				PlantName				nvarchar(64)	'$.Plant.Name',
				UserLogin				nvarchar(48)	'$.User.Login'
			) jsr

	inner join	dbo.Plant as p
	on			p.[Name] = jsr.PlantName

	inner join	dbo.[User] as u
	on			u.[Login] = jsr.UserLogin;
end
go