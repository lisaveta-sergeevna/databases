use PlantCatalog
go

if object_id('dbo.USP_ParseJson_RemoveReview', 'P') is not null
	drop procedure dbo.USP_ParseJson_RemoveReview
go

create procedure dbo.USP_ParseJson_RemoveReview
(
	@json nvarchar(max)
)
as
begin
	merge	dbo.Review as [Target]
	using	
	(
		select	jsr.PublicationDateTime as PublicationDateTime,
				u.UserId				as UserId
		from	openjson(@json)
				with
				(
					PublicationDateTime		datetime		'$.Review.PublicationDateTime',
					UserLogin				nvarchar(48)	'$.User.Login'
				) jsr

		inner join	dbo.[User] as u
		on			u.[Login] = jsr.UserLogin
	) as [Source]

	on	[Target].UserId					= [Source].UserId
	and [Target].PublicationDateTime	= [Source].PublicationDateTime

	when matched then
		delete; 
end
go