use PlantCatalog
go

if object_id('dbo.USP_ParseJson_AddClade', 'P') is not null
	drop procedure dbo.USP_ParseJson_AddClade
go

create procedure dbo.USP_ParseJson_AddClade
(
	@json nvarchar(max)
)
as
begin
	merge	dbo.Clade as [Target]
	using	
	(
		select	jsc.[Name]		as [Name],
				jsc.LatinName	as LatinName,
				g.GroupId		as GroupId
		from	openjson(@json)
				with
				(
					[Name]		nvarchar(64)	'$.Clade.Name',
					LatinName	nvarchar(64)	'$.Clade.LatinName',
					GroupName	nvarchar(64)	'$.Group.Name'
				) jsc
		inner join	dbo.[Group] as g
		on			g.[Name] = jsc.GroupName
	) as [Source]

	on	[Target].[Name] = [Source].[Name]

	when matched then
		update
		set [Target].LatinName	= [Source].LatinName,
			[Target].GroupId	= [Source].GroupId

	when not matched then
		insert
		(
			[Name],
			LatinName,
			GroupId
		)
		values
		(
			[Name],
			LatinName,
			GroupId
		);
end
go