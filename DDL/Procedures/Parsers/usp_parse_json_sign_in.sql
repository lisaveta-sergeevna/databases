use PlantCatalog
go

if object_id('dbo.USP_ParseJson_SignIn', 'P') is not null
	drop procedure dbo.USP_ParseJson_SignIn
go

create procedure dbo.USP_ParseJson_SignIn
(
	@json nvarchar(max)
)
as
begin
	declare @resultTable table (id uniqueidentifier);

	merge	dbo.[User] as [Target]
	using	
	(
		select	jsu.[Login]			as [Login],
				jsu.PasswordHash	as PasswordHash
		from	openjson(@json)
				with
				(
					[Login]			nvarchar(48)	'$.User.Login',
					PasswordHash	char(64)		'$.User.PasswordHash'
				) jsu
	) as [Source]

	on		[Target].[Login]		= [Source].[Login]
	and		[Target].PasswordHash	= [Source].PasswordHash

	when matched then
		update set [Target].LastEntryDate = getdate()

	output inserted.UserId into @resultTable;

	if (select * from @resultTable) is not null
		return 0
	else
		return 1

end
go