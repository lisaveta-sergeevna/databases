use PlantCatalog
go

if object_id('dbo.USP_ParseJson_AddFamily', 'P') is not null
	drop procedure dbo.USP_ParseJson_AddFamily
go

create procedure dbo.USP_ParseJson_AddFamily
(
	@json nvarchar(max)
)
as
begin
	merge	dbo.Family as [Target]
	using	
	(
		select	jsf.[Name]		as [Name],
				jsf.LatinName	as LatinName,
				c.CladeId		as CladeId
		from	openjson(@json)
				with
				(
					[Name]		nvarchar(64)	'$.Family.Name',
					LatinName	nvarchar(64)	'$.Family.LatinName',
					CladeName	nvarchar(64)	'$.Clade.Name'
				) jsf
		inner join	dbo.[Clade] as c
		on			c.[Name] = jsf.CladeName
	) as [Source]

	on	[Target].[Name] = [Source].[Name]

	when matched then
		update
		set [Target].LatinName	= [Source].LatinName,
			[Target].CladeId	= [Source].CladeId

	when not matched then
		insert
		(
			[Name],
			LatinName,
			CladeId
		)
		values
		(
			[Name],
			LatinName,
			CladeId
		);
end
go