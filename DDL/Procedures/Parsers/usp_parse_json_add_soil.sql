use PlantCatalog
go

if object_id('dbo.USP_ParseJson_AddSoil', 'P') is not null
    drop procedure dbo.USP_ParseJson_AddSoil
go

create proc dbo.USP_ParseJson_AddSoil
(
    @json nvarchar(max)
)
as
begin
    insert into dbo.Soil
    (
        [Name], 
        [Description]
    )
    select  jss.SoilName		as [Name],
            jss.[Description]	as [Description]
    from    openjson(@json)
            with
            (
                SoilName        nvarchar(64)    '$.Name',
                [Description]   nvarchar(1024)  '$.Description'
             ) jss
    left join   dbo.Soil
    on          [Name] = dbo.Soil.[Name]
    where       dbo.Soil.[Name] is null
end
go