use PlantCatalog
go

if object_id('dbo.USP_ParseJson_AddGroup', 'P') is not null
	drop procedure dbo.USP_ParseJson_AddGroup
go

create procedure dbo.USP_ParseJson_AddGroup
(
	@json nvarchar(max)
)
as
begin
	insert into dbo.[Group] 
	(
		[Name], 
		LatinName
	)
	select	jsg.GroupName as [Name],
			jsg.LatinName as LatinName
	from	openjson(@json)
			with
			(
				GroupName	nvarchar(64)	'$.Name',
				LatinName	nvarchar(64)	'$.LatinName'
			) jsg
	left join	dbo.[Group]
	on			[Name] = dbo.[Group].[Name]
	where		dbo.[Group].[Name] is null
end
go