use PlantCatalog
go

if object_id('dbo.USP_ParseJson_SignUp', 'P') is not null
	drop procedure dbo.USP_ParseJson_SignUp
go

create procedure dbo.USP_ParseJson_SignUp
(
	@json nvarchar(max)
)
as
begin
	declare @resultTable table (result int);

	merge	dbo.[User] as [Target]
	using		
	(
		select	jsu.[Name]			as [Name],
				jsu.Email			as Email,
				jsu.[Login]			as [Login],
				jsu.PasswordHash	as PasswordHash,
				r.RoleId			as RoleId
		from	openjson(@json)
				with
				(
					[Name]			nvarchar(48)	'$.User.Name',
					Email			nvarchar(64)	'$.User.Email',
					[Login]			nvarchar(48)	'$.User.Login',
					PasswordHash	char(64)		'$.User.PasswordHash',
					RoleName		nvarchar(24)	'$.Role.Name'
				) jsu
		inner join	dbo.[Role] as r
		on			r.[Name] = jsu.RoleName
	) as [Source]

	on	[Target].[Login] = [Source].[Login]

	when not matched then
		insert
		(
			[Name], 
			Email, 
			[Login], 
			PasswordHash,
			RoleId
		)
		values
		(
			[Name], 
			Email, 
			[Login], 
			PasswordHash,
			RoleId
		)

	output 1 into @resultTable;

	if (select * from @resultTable) is not null
		return 0
	else
		return 1
end
go