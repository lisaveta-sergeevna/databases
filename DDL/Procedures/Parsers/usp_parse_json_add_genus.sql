use PlantCatalog
go

if object_id('dbo.USP_ParseJson_AddGenus', 'P') is not null
	drop procedure dbo.USP_ParseJson_AddGenus
go

create procedure dbo.USP_ParseJson_AddGenus
(
	@json nvarchar(max)
)
as
begin
	merge	dbo.Genus as [Target]
	using	
	(
		select	jsg.[Name]		as [Name],
				jsg.LatinName	as LatinName,
				f.FamilyId		as FamilyId
		from	openjson(@json)
				with
				(
					[Name]		nvarchar(64)	'$.Genus.Name',
					LatinName	nvarchar(64)	'$.Genus.LatinName',
					FamilyName	nvarchar(64)	'$.Family.Name'
				) jsg
		inner join	dbo.[Family] as f
		on			f.[Name] = jsg.FamilyName
	) as [Source]

	on	[Target].[Name] = [Source].[Name]

	when matched then
		update
		set [Target].LatinName	= [Source].LatinName,
			[Target].FamilyId	= [Source].FamilyId

	when not matched then
		insert
		(
			[Name],
			LatinName,
			FamilyId
		)
		values
		(
			[Name],
			LatinName,
			FamilyId
		);
end
go