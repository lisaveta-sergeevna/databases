use PlantCatalog
go

if object_id('dbo.USP_ParseJson_AddRole', 'P') is not null
	drop procedure dbo.USP_ParseJson_AddRole
go

create procedure dbo.USP_ParseJson_AddRole
(
	@json nvarchar(max)
)
as
begin
	insert into dbo.[Role] ([Name])
		select	jsr.RoleName as [Name]
		from	openjson(@json)
				with
				(
					RoleName nvarchar(24) '$.Name'
				) jsr
		left join	dbo.[Role]
		on			[Name] = dbo.[Role].[Name]
		where		dbo.[Role].[Name] is null
end
go