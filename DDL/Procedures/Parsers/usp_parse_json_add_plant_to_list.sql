use PlantCatalog
go

if object_id('dbo.USP_ParseJson_AddPlantToList', 'P') is not null
	drop procedure dbo.USP_ParseJson_AddPlantToList
go

create procedure dbo.USP_ParseJson_AddPlantToList
(
	@json nvarchar(max)
)
as
begin
	merge	dbo.UserPlant as [Target]
	using	
	(
		select	jsp.Number		as Number,
				jsp.PlantDate	as PlantDate,
				jsp.[Name]		as [Name],
				jsp.PotVolume	as PotVolume,
				p.PlantId		as PlantId,
				u.UserId		as UserId
		from	openjson(@json)
				with
				(
					Number		tinyint			'$.UserPlant.Number',
					PlantDate	date			'$.UserPlant.PlantDate',
					[Name]		nvarchar(48)	'$.UserPlant.Name',
					PotVolume	float			'$.UserPlant.PotVolume',
					PlantName	nvarchar(64)	'$.Plant.Name',
					UserLogin	nvarchar(48)	'$.User.Login'
				) jsp

		inner join	dbo.Plant as p
		on			p.[Name] = jsp.PlantName

		inner join	dbo.[User] as u
		on			u.[Login] = jsp.UserLogin
	) as [Source]

	on	[Target].UserId		= [Source].UserId
	and [Target].PlantId	= [Source].PlantId
	and [Target].Number		= [Source].Number

	when matched then
		update
		set [Target].PlantDate	= [Source].PlantDate,
			[Target].[Name]		= [Source].[Name],
			[Target].PotVolume	= [Source].PotVolume

	when not matched then
		insert
		(
			Number,
			PlantDate,
			[Name],
			PotVolume,
			PlantId,
			UserId
		)
		values
		(
			Number,
			PlantDate,
			[Name],
			PotVolume,
			PlantId,
			UserId
		);
end
go