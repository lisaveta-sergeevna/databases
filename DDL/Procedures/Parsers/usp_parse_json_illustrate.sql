use PlantCatalog
go

if object_id('dbo.USP_ParseJson_Illustrate', 'P') is not null
	drop procedure dbo.USP_ParseJson_Illustrate
go

create procedure dbo.USP_ParseJson_Illustrate
(
	@json nvarchar(max)
)
as
begin
	merge	dbo.[Image] as [Target]
	using		
	(
		select	jsi.[Url]	as [Url],
				p.PlantId	as PlantId
		from	openjson(@json)
				with
				(
					[Url]		nvarchar(64)	'$.Image.Url',
					PlantName	nvarchar(64)	'$.Plant.Name'
				) jsi
		inner join	dbo.Plant as p
		on			p.[Name] = jsi.PlantName
	) as [Source]

	on	[Target].[Url] = [Source].[Url]

	when matched then
		update
		set [Target].PlantId = [Source].PlantId

	when not matched then
		insert
		(
			PlantId,
			[Url]
		)
		values
		(
			PlantId,
			[Url]
		);
end
go