use PlantCatalog
go

if object_id('dbo.USP_ParseJson_AddPlant', 'P') is not null
	drop procedure dbo.USP_ParseJson_AddPlant
go

create procedure dbo.USP_ParseJson_AddPlant
(
	@json nvarchar(max)
)
as
begin
	merge	dbo.Plant as [Target]
	using	
	(
		select	jsp.[Name]						as [Name],
				jsp.LatinName					as LatinName,
				jsp.Illumination				as Illumination,
				jsp.AirHumidity					as AirHumidity,
				jsp.Temperature					as Temperature,
				jsp.RecommendedWateringPeriod	as RecommendedWateringPeriod,
				jsp.RecommendedWateringVolume	as RecommendedWateringVolume,
				g.GenusId as GenusId
		from	openjson(@json)
				with
				(
					[Name]						nvarchar(64)	'$.Plant.Name',
					LatinName					nvarchar(64)	'$.Plant.LatinName',
					Illumination				nvarchar(512)	'$.Plant.Illumination',
					AirHumidity					float			'$.Plant.AirHumidity',
					Temperature					float			'$.Plant.Temperature',
					RecommendedWateringPeriod	tinyint			'$.Plant.RecommendedWateringPeriod',
					RecommendedWateringVolume	tinyint			'$.Plant.RecommendedWateringVolume',
					GenusName					nvarchar(64)	'$.Genus.Name'
				) jsp
		inner join	dbo.[Genus] as g
		on			g.[Name] = jsp.GenusName
	) as [Source]

	on	[Target].[Name] = [Source].[Name]

	when matched then
		update
		set [Target].LatinName					= [Source].LatinName,
			[Target].Illumination				= [Source].Illumination,
			[Target].AirHumidity				= [Source].AirHumidity,
			[Target].Temperature				= [Source].Temperature,
			[Target].RecommendedWateringPeriod	= [Source].RecommendedWateringPeriod,
			[Target].RecommendedWateringVolume	= [Source].RecommendedWateringVolume,
			[Target].GenusId					= [Source].GenusId

	when not matched then
		insert
		(
			[Name],
			LatinName,
			Illumination,
			AirHumidity,
			Temperature,
			RecommendedWateringPeriod,
			RecommendedWateringVolume,
			GenusId
		)
		values
		(
			[Name],
			LatinName,
			Illumination,
			AirHumidity,
			Temperature,
			RecommendedWateringPeriod,
			RecommendedWateringVolume,
			GenusId
		);
end
go