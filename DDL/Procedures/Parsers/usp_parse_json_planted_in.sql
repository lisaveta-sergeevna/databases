use PlantCatalog
go

if object_id('dbo.USP_ParseJson_PlantedIn', 'P') is not null
    drop procedure dbo.USP_ParseJson_PlantedIn
go

create proc dbo.USP_ParseJson_PlantedIn
(
    @json nvarchar(max)
)
as
begin
    merge   dbo.PlantSoilBridge as [Target]
    using       
	(
        select      p.PlantId		as PlantId,
                    s.SoilId		as SoilId,
                    jsp.[Percent]	as [Percent]        
        from        openjson(@json)
                    with
                    (
                        PlantName   nvarchar(64)    '$.Plant.Name',
                        SoilName    nvarchar(64)    '$.Soil.Name',
                        [Percent]   float		    '$.Soil.Percent'
                    ) jsp

        inner join  dbo.Plant as p
        on          p.[Name] = jsp.PlantName

        inner join  dbo.Soil as s
        on          s.[Name] = jsp.SoilName
    ) as [Source]

    on      [Target].PlantId	= [Source].PlantId
    and		[Target].SoilId		= [Source].SoilId

    when matched then
        update
        set [Target].[Percent] = [Source].[Percent]

    when not matched then
        insert
        (
            PlantId,
            SoilId,
            [Percent]
        )
        values
        (
            PlantId,
            SoilId,
            [Percent]
        );
end
go