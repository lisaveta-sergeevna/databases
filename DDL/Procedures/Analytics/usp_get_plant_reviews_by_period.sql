use PlantCatalog
go

if object_id('dbo.USP_GetPlantReviewsByPeriods', 'P') is not null
    drop procedure dbo.USP_GetPlantReviewsByPeriods
go

create proc dbo.USP_GetPlantReviewsByPeriods
(
    @json nvarchar(max)
)
as
begin
    create table #JsonParams
    (
		Plants				nvarchar(max),
		GenusId				uniqueidentifier,
		FamilyId			uniqueidentifier,
        PeriodStart			date,
        PeriodEnd	        date,
		ShowByMonths		bit,
        ShowTotalPlants	    bit,
        ShowMaxUserPlants   bit,
		ShowZeroColumns		bit,
		ShowZeroRows		bit
    )

    insert into #JsonParams
                (
					Plants,
					GenusId,
					FamilyId,
					PeriodStart,
					PeriodEnd,
					ShowByMonths,
					ShowTotalPlants,
					ShowMaxUserPlants,
					ShowZeroColumns,
					ShowZeroRows
                )
    select      coalesce(jsp.Plants, '[]'),
				g.GenusId,
				f.FamilyId,
				coalesce(iif(len(jsp.PeriodStart) = 4,
							convert(date, jsp.PeriodStart + '-01-01'),
							convert(date, jsp.PeriodStart + '-01')), 
						(select min(PublicationDateTime) from dbo.Review)),
                coalesce(iif(len(jsp.PeriodStart) = 4,
							dateadd(year, 1, convert(date, jsp.PeriodEnd + '-01-01')), 
							dateadd(month, 1, convert(date, jsp.PeriodEnd +'-01'))),
						getdate()),
				iif(len(jsp.PeriodStart) = 4, 0, 1),
                coalesce(jsp.ShowTotalPlants, 1),
                coalesce(jsp.ShowMaxUserPlants, 1),
				coalesce(jsp.ShowZeroColumns, 1),
				coalesce(jsp.ShowZeroRows, 0)
    from        openjson(@json)
                with
                (
					Plants				nvarchar(max)	'$.Plants' as json,
					GenusName			nvarchar(64)	'$.Genus.Name',
					FamilyName			nvarchar(64)	'$.Family.Name',
					PeriodStart			nvarchar(7)		'$.PeriodStart',
					PeriodEnd	        nvarchar(7)		'$.PeriodEnd',
					ShowTotalPlants	    bit				'$.ShowTotalPlants',
					ShowMaxUserPlants   bit				'$.ShowMaxUserPlants',
					ShowZeroColumns		bit				'$.ShowZeroColumns',
					ShowZeroRows		bit				'$.ShowZeroRows'
                ) jsp

				left join	dbo.Genus	g
				on			jsp.GenusName	= g.[Name]

				left join	dbo.Family	f
				on			jsp.FamilyName	= f.[Name]

	create table #PivotSource
    ( 
		Family		nvarchar(64),
		Genus		nvarchar(64),
        [Name]		nvarchar(64),
		[Date]		nvarchar(7),
        ReviewId	uniqueidentifier
    )

	;with cte_Plants
	(	
		Family, 
		Genus, 
		PlantId, 
		[Name]
	)
	as
	(
		select	f.[Name],
				g.[Name],
				p.PlantId,
				p.[Name]
		from		dbo.Plant	as p
		inner join	dbo.Genus	as g
		on			g.GenusId	= p.GenusId
		inner join	dbo.Family	as f
		on			f.FamilyId	= g.FamilyId

		cross join	#JsonParams as jp
		outer apply openjson(jp.Plants)
					with
					(
						[Name]	nvarchar(64)	'$.Name'
					) jsp
		where	p.[Name]	= jsp.[Name]
		or		g.GenusId	= jp.GenusId
		or		f.FamilyId	= jp.FamilyId

	),
	cte_ReviewPlantStat
	(
		PlantId, 
		[Name], 
		[Date], 
		ReviewId
	)
	as
	(
		select	p.PlantId,
				p.[Name],
				iif(jp.ShowByMonths = 1,
					convert(nvarchar(7), r.PublicationDateTime), 
					convert(nvarchar(4), r.PublicationDateTime)),
				r.ReviewId
		from		cte_Plants		as p
		left join	dbo.Review		as r
		on			r.PlantId	= p.PlantId
		cross join	#JsonParams as jp
		where		r.PublicationDateTime
			between jp.PeriodStart
			and		jp.PeriodEnd
		or			r.PublicationDateTime is null
	)
	insert into #PivotSource
	(
		Family,
		Genus,
		[Name],
		[Date],
		ReviewId
	)
	select	p.Family	as Family,
			p.Genus		as Genus,
			r.[Name]	as [Name],
			r.[Date]	as [Date],
			ReviewId	as ReviewId
	from		cte_ReviewPlantStat	as r
	left join	cte_Plants			as p
	on			r.PlantId = p.PlantId

	declare @datesQuery			nvarchar(max)
	declare	@checkForReviews	bit
	declare @periods			nvarchar(max)

	;if exists(select * from #JsonParams where ShowZeroColumns = 0)
		set @checkForReviews = 1

	;if exists(select * from #JsonParams where ShowByMonths = 1)
		with cte_Months
		(
			[Month]
		)
		as
		(
			select		jp.PeriodStart [Month]
			from		#JsonParams as jp
			union all
			select		dateadd(month, 1, [Month])
			from		cte_Months	as cte
			cross join	#JsonParams as jp
			where		dateadd(month, 1, cte.[Month]) < jp.PeriodEnd
		),
		cte_DistinctMonths
		(
			[Month]
		)
		as
		(
			select distinct	iif(jp.ShowZeroColumns = 1, m.[Month], convert(date, ps.[Date] + '-01'))
			from			cte_Months		as m
			left join		#PivotSource	as ps
			on				convert(date, ps.[Date] + '-01') = m.[Month]
			cross join		#JsonParams		as jp
		)
		select  @periods = coalesce(@periods + ', ', '') + '[' + convert(nvarchar(7), [Month])+ ']'
		from    cte_DistinctMonths
	else
		with cte_Years
		(
			[Year]
		)
		as
		(
			select		cast(year(jp.PeriodStart) as smallint) [Year]
			from        #JsonParams as jp
			union all
			select      cast((cte.[Year] + 1) as smallint)
			from        cte_Years cte
			cross join  #JsonParams as jp
			where       cte.[Year] + 1 < year(jp.PeriodEnd)
		),
		cte_DistinctYears
		(
			[Year]
		)
		as
		(
			select distinct iif(jp.ShowZeroColumns = 1, y.[Year], year(ps.[Date]))
			from            cte_Years		as y
			left join       #PivotSource	as ps
			on			    year(ps.[Date]) = y.[Year]
			cross join      #JsonParams		as jp
		)
		select  @periods = coalesce(@periods + ', ', '') + '[' + convert(nvarchar(4), [Year])+ ']'
		from    cte_DistinctYears

	declare @query			nvarchar(max)
	declare @columns		nvarchar(128)
	declare @rowsCondition	nvarchar(max)

	if exists(select * from #JsonParams where ShowTotalPlants = 1)
		set @columns = coalesce(@columns, '') + 'TotalPlants, '

	if exists(select * from #JsonParams where ShowMaxUserPlants = 1)
		set @columns = coalesce(@columns, '') + 'MaxAmount, '

	if exists(select * from #JsonParams where ShowZeroRows = 1)
		set @rowsCondition = ''
	else
		set @rowsCondition = 'where	ReviewId is not null'

    set @query = 
    '	
	;with cte_UserPlantStat
	(
		PlantName, 
		TotalPlants, 
		MaxAmount
	)
	as
	(
		select	[Name],
				sum(Total),  
				max(Total)
		from
		(
			select	p.[Name], 
					count(up.UserPlantId) as Total
			from		Plant		as p

			left join	UserPlant	as up
			on			up.PlantId = p.PlantId

			group by p.[Name], up.UserId
		) as subquery

		group by [Name]
	)
    select  Family,
			Genus,
			[Name],
			' + coalesce(@columns, '') + '
            ' + @periods + '
    from    
    (
        select  Family,
				Genus,
				[Name],
				[Date],
				ReviewId
        from    #PivotSource 
		' + @rowsCondition + '
    ) src
    pivot
    (
		count(ReviewId)
        for     [Date]
        in      (' + @periods + ')
    ) pvt

	inner join cte_UserPlantStat	as cte
		on cte.PlantName = pvt.[Name]
    '
    exec(@query)
end
go