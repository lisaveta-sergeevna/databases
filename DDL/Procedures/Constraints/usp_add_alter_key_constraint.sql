use PlantCatalog
go

if object_id(N'dbo.USP_AddAlterKeyConstraint', N'P') is not null
	drop procedure dbo.USP_AddAlterKeyConstraint
go

create procedure dbo.USP_AddAlterKeyConstraint
(
	@table			nvarchar(128),
	@constraint		nvarchar(64),
	@attributes		nvarchar(400)
)
as
begin
	declare @query nvarchar(max)

	set @query = 
	'
	if object_id(N''{constraint}'', N''UQ'') is not null
		alter table {table} drop constraint {constraint};

	alter table {table} add constraint {constraint} unique ({attributes});
	'

	set @query = replace(@query, '{table}',			@table)
    set @query = replace(@query, '{attributes}',	@attributes)
    set @query = replace(@query, '{constraint}',    @constraint)

	exec(@query)
end
go