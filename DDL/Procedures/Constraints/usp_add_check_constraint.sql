use PlantCatalog
go

if object_id(N'dbo.USP_AddCheckConstraint', N'P') is not null
	drop procedure dbo.USP_AddCheckConstraint
go

create procedure dbo.USP_AddCheckConstraint
(
	@table			nvarchar(128),
	@constraint		nvarchar(64),
	@checkClause	nvarchar(256)
)
as
begin
	declare @query nvarchar(max)

	set @query = 
	'
	if object_id(N''{constraint}'', N''C'') is not null
		alter table {table} drop constraint {constraint};

	alter table {table} add constraint {constraint} check {checkClause};
	'

	set @query = replace(@query, '{table}',			@table)
    set @query = replace(@query, '{constraint}',    @constraint)
	set @query = replace(@query, '{checkClause}',	@checkClause)

	exec(@query)
end
go