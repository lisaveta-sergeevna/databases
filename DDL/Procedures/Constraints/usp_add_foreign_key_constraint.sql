use PlantCatalog
go

if object_id(N'dbo.USP_AddForeignKeyConstraint', N'P') is not null
	drop procedure dbo.USP_AddForeignKeyConstraint
go

create procedure dbo.USP_AddForeignKeyConstraint
(
	@dependentTable			nvarchar(128),
	@depententAttributes	nvarchar(400),
	@referenceTable			nvarchar(128),
	@referenceAttributes	nvarchar(400),
	@constraint				nvarchar(64)
)
as
begin
	declare @query nvarchar(max)

	set @query = 
	'
	if object_id(N''{constraint}'', N''F'') is not null 
		alter table {dependentTable} drop constraint {constraint};

	alter table {dependentTable} add constraint {constraint} 
		foreign key ({depententAttributes}) 
		references {referenceTable} ({referenceAttributes}) 
		on delete cascade;
	'

	set @query = replace(@query, '{dependentTable}',		@dependentTable)
    set @query = replace(@query, '{depententAttributes}',	@depententAttributes)
	set @query = replace(@query, '{referenceTable}',		@referenceTable)
	set @query = replace(@query, '{referenceAttributes}',	@referenceAttributes)
    set @query = replace(@query, '{constraint}',			@constraint)

	exec(@query)
end
go