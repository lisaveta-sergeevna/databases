use PlantCatalog
go

if object_id(N'dbo.USP_AddDefaultConstraint', N'P') is not null
	drop procedure dbo.USP_AddDefaultConstraint
go

create procedure dbo.USP_AddDefaultConstraint
(
	@table			nvarchar(128),
	@constraint		nvarchar(64),
	@attribute		nvarchar(128),
	@value			nvarchar(128)
)
as
begin
	declare @query nvarchar(max)

	set @query = 
	'
	if object_id(N''{constraint}'', N''D'') is not null
		alter table {table} drop constraint {constraint};

	alter table {table} add constraint {constraint} default {value}
		for {attribute};
	'

	set @query = replace(@query, '{table}',			@table)
    set @query = replace(@query, '{attribute}',		@attribute)
    set @query = replace(@query, '{constraint}',    @constraint)
	set @query = replace(@query, '{value}',			@value)

	exec(@query)
end
go