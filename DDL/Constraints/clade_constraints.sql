use PlantCatalog
go

declare	@returnValue int

exec	@returnValue = dbo.USP_AddAlterKeyConstraint
		@table = N'dbo.Clade',
		@constraint = N'AK_Clade_LatinName',
		@attributes = N'LatinName'

exec	@returnValue = dbo.USP_AddAlterKeyConstraint
		@table = N'dbo.Clade',
		@constraint = N'AK_Clade_Name',
		@attributes = N'Name'

exec	@returnValue = dbo.USP_AddForeignKeyConstraint
		@dependentTable = N'dbo.Clade',
		@depententAttributes = N'GroupId',
		@referenceTable = N'dbo.[Group]',
		@referenceAttributes = N'GroupId',
		@constraint = N'FK_Clade_Group'