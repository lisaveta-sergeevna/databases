use PlantCatalog
go

declare	@returnValue int

exec	@returnValue = dbo.USP_AddAlterKeyConstraint
		@table = N'dbo.Soil',
		@constraint = N'AK_Soil_Name',
		@attributes = N'Name'