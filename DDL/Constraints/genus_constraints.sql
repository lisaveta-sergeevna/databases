use PlantCatalog
go

declare @returnValue int

exec	@returnValue = dbo.USP_AddForeignKeyConstraint
		@dependentTable = N'dbo.Genus',
		@depententAttributes = N'FamilyId',
		@referenceTable = N'dbo.Family',
		@referenceAttributes = N'FamilyId',
		@constraint = N'FK_Genus_Family'

exec	@returnValue = dbo.USP_AddAlterKeyConstraint
		@table = N'dbo.Genus',
		@constraint = N'AK_Genus_Name',
		@attributes = N'Name'

exec	@returnValue = dbo.USP_AddAlterKeyConstraint
		@table = N'dbo.Genus',
		@constraint = N'AK_Genus_LatinName',
		@attributes = N'LatinName'