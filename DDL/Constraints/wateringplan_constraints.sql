use PlantCatalog
go

declare	@returnValue int

exec	@returnValue = dbo.USP_AddAlterKeyConstraint
		@table = N'dbo.WateringPlan',
		@constraint = N'AK_WateringPlan_UserPlantId_Number',
		@attributes = N'UserPlantId, Number'

exec	@returnValue = dbo.USP_AddCheckConstraint
		@table = N'dbo.WateringPlan',
		@constraint = N'CK_WateringPlan_WaterVolume',
		@checkClause = N'([WaterVolume] > 0)'

exec	@returnValue = dbo.USP_AddCheckConstraint
		@table = N'dbo.WateringPlan',
		@constraint = N'CK_WateringPlan_Period',
		@checkClause = N'([Period] > 0)'
		
exec	@returnValue = dbo.USP_AddCheckConstraint
		@table = N'dbo.WateringPlan',
		@constraint = N'CK_WateringPlan_Number',
		@checkClause = N'([Number] > 0)'

exec	@returnValue = dbo.USP_AddDefaultConstraint
		@table = N'dbo.WateringPlan',
		@constraint = N'DF_WateringPlan_WaterVolume',
		@attribute = N'WaterVolume',
		@value = N'100'

exec	@returnValue = dbo.USP_AddDefaultConstraint
		@table = N'dbo.WateringPlan',
		@constraint = N'DF_WateringPlan_StartDateTime',
		@attribute = N'StartDateTime',
		@value = N'getdate()'

exec	@returnValue = dbo.USP_AddDefaultConstraint
		@table = N'dbo.WateringPlan',
		@constraint = N'DF_WateringPlan_Period',
		@attribute = N'Period',
		@value = N'1'

exec	@returnValue = dbo.USP_AddDefaultConstraint
		@table = N'dbo.WateringPlan',
		@constraint = N'DF_WateringPlan_NotificationsEnabled',
		@attribute = N'NotificationsEnabled',
		@value = N'0'

exec	@returnValue = dbo.USP_AddForeignKeyConstraint
		@dependentTable = N'dbo.WateringPlan',
		@depententAttributes = N'UserPlantId',
		@referenceTable = N'dbo.UserPlant',
		@referenceAttributes = N'UserPlantId',
		@constraint = N'FK_WateringPlan_UserPlant'