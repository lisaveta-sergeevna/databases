use PlantCatalog
go

declare	@returnValue int

exec	@returnValue = dbo.USP_AddCheckConstraint
		@table = N'dbo.PlantSoilBridge',
		@constraint = N'CK_PlantSoilBridge_Percent',
		@checkClause = N'([Percent] between 0 and 100)'

exec	@returnValue = dbo.USP_AddForeignKeyConstraint
		@dependentTable = N'dbo.PlantSoilBridge',
		@depententAttributes = N'SoilId',
		@referenceTable = N'dbo.Soil',
		@referenceAttributes = N'SoilId',
		@constraint = N'FK_PlantSoilBridge_Soil'

exec	@returnValue = dbo.USP_AddForeignKeyConstraint
		@dependentTable = N'dbo.PlantSoilBridge',
		@depententAttributes = N'PlantId',
		@referenceTable = N'dbo.Plant',
		@referenceAttributes = N'PlantId',
		@constraint = N'FK_PlantSoilBridge_Plant'