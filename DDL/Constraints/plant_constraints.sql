use PlantCatalog
go

declare @returnValue int

exec	@returnValue = dbo.USP_AddForeignKeyConstraint
		@dependentTable = N'dbo.Plant',
		@depententAttributes = N'GenusId',
		@referenceTable = N'dbo.Genus',
		@referenceAttributes = N'GenusId',
		@constraint = N'FK_Plant_Genus'

exec	@returnValue = dbo.USP_AddDefaultConstraint
		@table = N'dbo.Plant',
		@constraint = N'DF_Plant_RecommendedWateringVolume',
		@attribute = N'RecommendedWateringVolume',
		@value = N'100'

exec	@returnValue = dbo.USP_AddDefaultConstraint
		@table = N'dbo.Plant',
		@constraint = N'DF_Plant_RecommendedWateringPeriod',
		@attribute = N'RecommendedWateringPeriod',
		@value = N'1'

exec	@returnValue = dbo.USP_AddCheckConstraint
		@table = N'dbo.Plant',
		@constraint = N'CK_Plant_Temperature',
		@checkClause = N'([Temperature] between -10 and 50)'

exec	@returnValue = dbo.USP_AddCheckConstraint
		@table = N'dbo.Plant',
		@constraint = N'CK_Plant_RecommendedWateringVolume',
		@checkClause = N'([RecommendedWateringVolume] > 0)'

exec	@returnValue = dbo.USP_AddCheckConstraint
		@table = N'dbo.Plant',
		@constraint = N'CK_Plant_AirHumidity',
		@checkClause = N'([AirHumidity] between 0 and 100)'

exec	@returnValue = dbo.USP_AddCheckConstraint
		@table = N'dbo.Plant',
		@constraint = N'CK_Plant_RecommendedWateringPeriod',
		@checkClause = N'([RecommendedWateringPeriod] > 0)'

exec	@returnValue = dbo.USP_AddAlterKeyConstraint
		@table = N'dbo.Plant',
		@constraint = N'AK_Plant_Name',
		@attributes = N'Name'

exec	@returnValue = dbo.USP_AddAlterKeyConstraint
		@table = N'dbo.Plant',
		@constraint = N'AK_Plant_LatinName',
		@attributes = N'LatinName'