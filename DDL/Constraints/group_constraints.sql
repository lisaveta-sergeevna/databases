use PlantCatalog
go

declare	@returnValue int

exec	@returnValue = dbo.USP_AddAlterKeyConstraint
		@table = N'dbo.[Group]',
		@constraint = N'AK_Group_Name',
		@attributes = N'Name'

exec	@returnValue = dbo.USP_AddAlterKeyConstraint
		@table = N'dbo.[Group]',
		@constraint = N'AK_Group_LatinName',
		@attributes = N'LatinName'