use PlantCatalog
go

declare	@returnValue int

exec	@returnValue = dbo.USP_AddAlterKeyConstraint
		@table = N'dbo.UserPlant',
		@constraint = N'AK_UserPlant_UserId_PlantId_Number',
		@attributes = N'UserId, PlantId, Number'

exec	@returnValue = dbo.USP_AddCheckConstraint
		@table = N'dbo.UserPlant',
		@constraint = N'CK_UserPlant_PotVolume',
		@checkClause = N'([PotVolume] > 0)'

exec	@returnValue = dbo.USP_AddCheckConstraint
		@table = N'dbo.UserPlant',
		@constraint = N'CK_UserPlant_Number',
		@checkClause = N'([Number] > 0)'

exec	@returnValue = dbo.USP_AddForeignKeyConstraint
		@dependentTable = N'dbo.UserPlant',
		@depententAttributes = N'UserId',
		@referenceTable = N'dbo.[User]',
		@referenceAttributes = N'UserId',
		@constraint = N'FK_UserPlant_User'

exec	@returnValue = dbo.USP_AddForeignKeyConstraint
		@dependentTable = N'dbo.UserPlant',
		@depententAttributes = N'PlantId',
		@referenceTable = N'dbo.Plant',
		@referenceAttributes = N'PlantId',
		@constraint = N'FK_UserPlant_Plant'