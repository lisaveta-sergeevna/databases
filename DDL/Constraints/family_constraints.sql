use PlantCatalog
go

declare @returnValue int

exec	@returnValue = dbo.USP_AddForeignKeyConstraint
		@dependentTable = N'dbo.Family',
		@depententAttributes = N'CladeId',
		@referenceTable = N'dbo.Clade',
		@referenceAttributes = N'CladeId',
		@constraint = N'FK_Family_Clade'

exec	@returnValue = dbo.USP_AddAlterKeyConstraint
		@table = N'dbo.Family',
		@constraint = N'AK_Family_LatinName',
		@attributes = N'LatinName'

exec	@returnValue = dbo.USP_AddAlterKeyConstraint
		@table = N'dbo.Family',
		@constraint = N'AK_Family_Name',
		@attributes = N'Name'