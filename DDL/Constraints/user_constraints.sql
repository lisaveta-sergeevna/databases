use PlantCatalog
go

declare	@returnValue int

exec	@returnValue = dbo.USP_AddAlterKeyConstraint
		@table = N'dbo.[User]',
		@constraint = N'AK_User_Login',
		@attributes = N'Login'

exec	@returnValue = dbo.USP_AddAlterKeyConstraint
		@table = N'dbo.[User]',
		@constraint = N'AK_User_Email',
		@attributes = N'Email'

exec	@returnValue = dbo.USP_AddDefaultConstraint
		@table = N'dbo.[User]',
		@constraint = N'DF_User_LastEntryDate',
		@attribute = N'LastEntryDate',
		@value = N'getdate()'

exec	@returnValue = dbo.USP_AddForeignKeyConstraint
		@dependentTable = N'dbo.[User]',
		@depententAttributes = N'RoleId',
		@referenceTable = N'dbo.[Role]',
		@referenceAttributes = N'RoleId',
		@constraint = N'FK_User_Role'