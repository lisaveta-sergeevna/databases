use PlantCatalog
go

declare	@returnValue int

exec	@returnValue = dbo.USP_AddAlterKeyConstraint
		@table = N'dbo.[Role]',
		@constraint = N'AK_Role_Name',
		@attributes = N'Name'