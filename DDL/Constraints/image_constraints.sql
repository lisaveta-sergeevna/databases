use PlantCatalog
go

declare	@returnValue int

exec	@returnValue = dbo.USP_AddAlterKeyConstraint
		@table = N'dbo.[Image]',
		@constraint = N'AK_Image_Url',
		@attributes = N'Url'

exec	@returnValue = dbo.USP_AddForeignKeyConstraint
		@dependentTable = N'dbo.[Image]',
		@depententAttributes = N'PlantId',
		@referenceTable = N'dbo.Plant',
		@referenceAttributes = N'PlantId',
		@constraint = N'FK_Image_Plant'