use PlantCatalog
go

declare	@returnValue int

exec	@returnValue = dbo.USP_AddAlterKeyConstraint
		@table = N'dbo.Review',
		@constraint = N'AK_Review_UserId_PlantId_PublicationDateTime',
		@attributes = N'UserId, PlantId, PublicationDateTime'

exec	@returnValue = dbo.USP_AddDefaultConstraint
		@table = N'dbo.Review',
		@constraint = N'DF_Review_PublicationDateTime',
		@attribute = N'PublicationDateTime',
		@value = N'sysdatetime()'

exec	@returnValue = dbo.USP_AddForeignKeyConstraint
		@dependentTable = N'dbo.Review',
		@depententAttributes = N'UserId',
		@referenceTable = N'dbo.[User]',
		@referenceAttributes = N'UserId',
		@constraint = N'FK_Review_User'

exec	@returnValue = dbo.USP_AddForeignKeyConstraint
		@dependentTable = N'dbo.Review',
		@depententAttributes = N'PlantId',
		@referenceTable = N'dbo.Plant',
		@referenceAttributes = N'PlantId',
		@constraint = N'FK_Review_Plant'